Source: sgb
Section: non-free/math
Priority: optional
Maintainer: Julian Gilbey <jdg@debian.org>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13),
               texlive-base,
               texlive-base-bin,
               texlive-extra-utils
Homepage: https://www-cs-faculty.stanford.edu/~knuth/sgb.html
Vcs-Git: https://salsa.debian.org/debian/sgb.git
Vcs-Browser: https://salsa.debian.org/debian/sgb
Rules-Requires-Root: no
XS-Autobuild: yes

Package: sgb
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: c-compiler, texlive-base, texlive-base-bin, texlive-extra-utils
Suggests: sgb-doc (>= ${binary:Version})
Description: The Stanford GraphBase: combinatorial data and algorithms
 A highly portable collection of programs and data for researchers who
 study combinatorial algorithms and data structures.
 .
 The programs are intended to be interesting in themselves as examples
 of literate programming.  Thus, the Stanford GraphBase can also be
 regarded as a collection of approximately 30 essays for programmers
 to enjoy reading, whether or not they are doing algorithmic research.
 The programs are written in CWEB, a combination of TeX and C that is
 easy to use by anyone who knows those languages and easy to read by
 anyone familiar with the rudiments of C.
 .
 This package contains only the libraries and the demonstration
 programs; for the readable source code, which forms the documentation
 as well, see the sgb-doc package.

Package: sgb-doc
Architecture: all
Section: non-free/doc
Depends: ${misc:Depends}
Recommends: sgb, texlive-base | texlive-base-bin
Replaces: sgb-src (<= 1:2000.09.14-4)
Conflicts: sgb (<< 1:20090810-1)
Description: Documentation for the Stanford GraphBase
 This package contains the source code for the GraphBase library, which
 forms the major documentation of the package.  It will be installed in
 /usr/share/doc/sgb/src.
 .
 The source code is written in CWEB, a combination of TeX and C, using a
 very readable literate programming style, making the code a joy to read.
