--- /dev/null
+++ b/Makefile.debian
@@ -0,0 +1,200 @@
+# -*- makefile -*-
+#   Makefile for the Stanford GraphBase
+#   Slightly modified by Milan Zamazal <pdm@fi.muni.cz> for building of Debian
+#   package.
+#   Trimmed down and modified for Debian builds.
+
+#   We use DEBROOT as the root of the Debian installation tree.  Note
+#   that we don't use mkdir anywhere in this makefile to create the
+#   directories, as that will already have been done by one of the
+#   debhelper programs.
+DEBROOT = debian/sgb
+
+#   Change DATADIR to the directory where GraphBase data files will go.
+#   We use INSTALLDATADIR for installation and DATADIR for compiling:
+INSTALLDATADIR = $(DEBROOT)/usr/share/sgb
+DATADIR = /usr/share/sgb
+
+#   Change INCLUDEDIR to the directory where GraphBase header files will go:
+INCLUDEDIR = $(DEBROOT)/usr/include
+
+#   Change LIBDIR to the directory where GraphBase library routines will go:
+LIBDIR = $(DEBROOT)/usr/lib
+
+#   Change BINDIR to the directory where installdemos will put demo programs:
+BINDIR = $(DEBROOT)/usr/bin
+
+#   Change CWEBINPUTS to the directory where CWEB include files will go:
+CWEBINPUTS = $(DEBROOT)/usr/share/texmf/cweb
+
+########## You shouldn't have to change anything after this point ##########
+
+#   Ditto for the order of the library search path.
+LDFLAGS += -L.
+LDLIBS += -lgb
+SOLIBS = libgb.so
+LOADLIBES = $(LDLIBS)
+
+.SUFFIXES: .dvi .tex .w
+
+.tex.dvi:
+	tex $*.tex
+
+.w.c:
+	if test -r $*.ch; then ctangle $*.w $*.ch; else ctangle $*.w; fi
+
+.w.tex:
+	if test -r $*.ch; then cweave $*.w $*.ch; else cweave $*.w; fi
+
+.w.o:
+	$(MAKE) -f Makefile.debian $*.c
+	$(MAKE) -f Makefile.debian $*.o
+
+.c:
+	$(CC) $(CFLAGS) $(LFLAGS) -o $* $(LDFLAGS) $< $(LDLIBS)
+
+.o:
+	$(CC) $(LFLAGS) -o $* $(LDFLAGS) $< $(LDLIBS)
+
+.w:
+	$(MAKE) -f Makefile.debian $*.c
+	$(MAKE) -f Makefile.debian $*
+
+.w.dvi:
+	$(MAKE) -f Makefile.debian $*.tex
+	$(MAKE) -f Makefile.debian $*.dvi
+
+DATAFILES = anna.dat david.dat econ.dat games.dat homer.dat huck.dat \
+        jean.dat lisa.dat miles.dat roget.dat words.dat
+KERNELFILES = gb_flip.w gb_graph.w gb_io.w gb_sort.w
+GENERATORFILES = gb_basic.w gb_books.w gb_econ.w gb_games.w gb_gates.w \
+        gb_lisa.w gb_miles.w gb_plane.w gb_raman.w gb_rand.w gb_roget.w \
+        gb_words.w
+DEMOFILES = assign_lisa.w book_components.w econ_order.w football.w \
+        girth.w ladders.w miles_span.w multiply.w queen.w roget_components.w \
+        take_risc.w word_components.w
+MISCWEBS = boilerplate.w gb_dijk.w gb_save.w gb_types.w test_sample.w
+CHANGEFILES = queen_wrap.ch word_giant.ch
+MISCFILES = Makefile README abstract.plaintex cities.texmap blank.w \
+        sample.correct test.correct test.dat +The+Stanford+GraphBase+
+ALL = $(DATAFILES) $(KERNELFILES) $(GENERATORFILES) $(DEMOFILES) \
+        $(MISCWEBS) $(CHANGEFILES) $(MISCFILES)
+
+OBJS = $(KERNELFILES:.w=.o) $(GENERATORFILES:.w=.o) gb_dijk.o gb_save.o
+HEADERS = $(OBJS:.o=.h)
+DEMOS = $(DEMOFILES:.w=)
+
+help:
+	@ echo "First 'make tests';"
+	@ echo "then (optionally) become superuser;"
+	@ echo "then 'make install';"
+	@ echo "then (optionally) 'make install-demos';"
+	@ echo "then (optionally) 'make clean'."
+
+lib: libgb.a
+
+solib: libgb.so
+
+libgb.so: $(OBJS)
+	rm -f socertified
+	# The -Wl,... stuff is to make the library a valid shared library,
+	# dynamically rather than statically linked, and having the right
+	# output to the file command and having a correct SONAME.
+	gcc -shared -o libgb.so $(OBJS) -Wl,-soname,libgb.so.1
+
+libgb.a: $(OBJS)
+	rm -f certified
+	ar rcv libgb.a $(OBJS)
+	- ranlib libgb.a
+
+demos: solib $(DEMOS)
+
+gb_io.o: gb_io.c
+	$(CC) $(CFLAGS) -DDATA_DIRECTORY=\"$(DATADIR)/\" -c gb_io.c
+
+test_io: gb_io.o
+	$(CC) $(CFLAGS) test_io.c gb_io.o -o test_io
+
+test_graph: gb_graph.o
+	$(CC) $(CFLAGS) test_graph.c gb_graph.o -o test_graph
+
+test_flip: gb_flip.o
+	$(CC) $(CFLAGS) test_flip.c gb_flip.o -o test_flip
+
+test_sample_static: test_sample.c
+	$(CC) $(CFLAGS) -o test_sample_static test_sample.c $(LDFLAGS) libgb.a
+
+tests: test_io test_graph test_flip
+	./test_io
+	./test_graph
+	./test_flip
+	$(MAKE) -f Makefile.debian gb_sort.o
+	$(MAKE) -f Makefile.debian lib
+	$(MAKE) -f Makefile.debian test_sample_static
+	- ./test_sample_static > sample.out
+	diff test.gb test.correct
+	diff sample.out sample.correct
+	rm test.gb sample.out test_io test_graph test_flip test_sample_static
+	echo "Congratulations --- the tests have all been passed."
+	touch certified
+
+sotests: test_io test_graph test_flip
+	./test_io
+	./test_graph
+	./test_flip
+	$(MAKE) -f Makefile.debian gb_sort.o
+	$(MAKE) -f Makefile.debian solib
+	$(MAKE) -f Makefile.debian test_sample
+	- LD_LIBRARY_PATH=. LD_PRELOAD=./libgb.so ./test_sample > sample.out
+	diff test.gb test.correct
+	diff sample.out sample.correct
+	rm test.gb sample.out test_io test_graph test_flip test_sample
+	echo "Congratulations --- the sotests have all been passed."
+	touch socertified
+
+install-libs:
+	if test ! -r certified; then echo "Please run 'make tests' first!"; fi
+	test -r certified
+	if test ! -r socertified; then echo "Please run 'make sotests' first!"; fi
+	test -r socertified
+	- cp libgb.a libgb.so $(LIBDIR)
+	- cp -p boilerplate.w gb_types.w $(CWEBINPUTS)
+	- cp -p $(HEADERS) $(INCLUDEDIR)
+
+install-data: $(DATAFILES)
+	- cp -p $(DATAFILES) $(INSTALLDATADIR)
+
+install-demos: demos
+	- mv $(DEMOS) $(BINDIR)
+
+uninstall-demos:
+	- cd $(BINDIR); rm -f $(DEMOS)
+
+install: install-libs install-data install-demos
+
+doc:
+	pdftex abstract.plaintex
+	pdftex cities.texmap
+
+clean:
+	rm -f *~ *.o *.c *.h libgb.a libgb.so certified socertified \
+	         *.tex *.log *.dvi *.toc *.idx *.scn *.ps *.pdf core
+
+veryclean: clean
+	rm -f $(DEMOS)
+
+sgb.tar: $(ALL)
+	tar cvf sgb.tar $(ALL)
+
+floppy: $(ALL)
+	bar cvf /dev/rfd0 $(ALL)
+	bar tvf /dev/rfd0
+	eject
+
+fullfloppy: $(ALL) ERRATA ANSI AMIGA PROTOTYPES
+	bar cvf /dev/rfd0 $(ALL) ERRATA ANSI AMIGA PROTOTYPES
+	bar tvf /dev/rfd0
+	eject
+
+fulltar: $(ALL) ERRATA ANSI AMIGA PROTOTYPES
+	tar cvf sgb.tar $(ALL) ERRATA ANSI AMIGA PROTOTYPES
--- /dev/null
+++ b/Makefile.doc
@@ -0,0 +1,49 @@
+# -*- makefile -*-
+#   Makefile for the Stanford GraphBase
+#   Stripped down for doc generation only
+
+.SUFFIXES: .dvi .tex .w
+
+.tex.dvi:
+	tex $*.tex
+
+.tex.pdf:
+	pdftex $*.tex
+
+.w.c:
+	if test -r $*.ch; then ctangle $*.w $*.ch; else ctangle $*.w; fi
+
+.w.tex:
+	if test -r $*.ch; then cweave $*.w $*.ch; else cweave $*.w; fi
+
+.w.dvi:
+	$(MAKE) $*.tex
+	$(MAKE) $*.dvi
+
+.w.pdf:
+	$(MAKE) $*.tex
+	$(MAKE) $*.pdf
+
+KERNELFILES = gb_flip.w gb_graph.w gb_io.w gb_sort.w
+GENERATORFILES = gb_basic.w gb_books.w gb_econ.w gb_games.w gb_gates.w \
+        gb_lisa.w gb_miles.w gb_plane.w gb_raman.w gb_rand.w gb_roget.w \
+        gb_words.w
+DEMOFILES = assign_lisa.w book_components.w econ_order.w football.w \
+        girth.w ladders.w miles_span.w multiply.w queen.w roget_components.w \
+        take_risc.w word_components.w
+MISCWEBS = gb_dijk.w gb_save.w
+CHANGEFILES = queen_wrap.ch word_giant.ch
+ALMOSTALL = $(KERNELFILES) $(GENERATORFILES) $(DEMOFILES) $(MISCWEBS)
+
+DVIS = $(ALMOSTALL:.w=.dvi) $(CHANGEFILES:.ch=.dvi)
+
+all doc: $(DVIS)
+
+clean:
+	rm -f *~ *.tex *.log *.dvi *.toc *.idx *.scn *.ps
+
+queen_wrap.tex: queen.w queen_wrap.ch
+	cweave queen.w queen_wrap.ch queen_wrap.tex
+
+word_giant.tex: word_components.w word_giant.ch
+	cweave word_components.w word_giant.ch word_giant.tex
